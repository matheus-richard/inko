# AST types message sends.
import std::compiler::ast::node::*
import std::compiler::ast::variables::*
import std::compiler::config::INSTRUCTION_RECEIVER_NAME
import std::compiler::source_location::SourceLocation
import std::compiler::symbol_table::Symbol
import std::compiler::types::(BlockType, Type)

# A keyword argument and its value.
object KeywordArgument {
  # The name of the keyword argument.
  @name: Identifier

  # The value of the argument.
  @value: Node

  # The source location of the send operation.
  @location: SourceLocation

  static def new(
    name: Identifier,
    value: Node,
    location: SourceLocation
  ) -> Self {
    Self { @name = name, @value = value, @location = location }
  }

  # Returns the name of the keyword argument.
  def name -> Identifier {
    @name
  }

  # Returns the value of the keyword argument.
  def value -> Node {
    @value
  }
}

impl Node for KeywordArgument {
  def location -> SourceLocation {
    @location
  }

  def resolved_type -> ?Type {
    @value.resolved_type
  }
}

# A message sent to a receiver.
object Send {
  # The name of the message that is sent.
  @message: String

  # The receiver the message is sent to.
  @receiver: Node

  # The arguments passed with the message.
  @arguments: Array!(Node)

  # Any type arguments to assign to type parameters.
  @type_arguments: Array!(TypeNode)

  # The source location of the send operation.
  @location: SourceLocation

  # The type returned by this message.
  @resolved_type: ?Type

  # The type thrown by this message.
  @resolved_throw_type: ?Type

  # The block type of this message.
  @block_type: ?BlockType

  # A boolean indicating that this node resides in a `try` expression.
  @inside_try: Boolean

  static def new(
    message: String,
    receiver: Node,
    arguments: Array!(Node),
    type_arguments: Array!(TypeNode),
    location: SourceLocation
  ) -> Self {
    Self {
      @message = message,
      @receiver = receiver,
      @arguments = arguments,
      @type_arguments = type_arguments,
      @location = location,
      @resolved_type = Nil,
      @resolved_throw_type = Nil,
      @block_type = Nil,
      @inside_try = False
    }
  }

  def message -> String {
    @message
  }

  def receiver -> Node {
    @receiver
  }

  def arguments -> Array!(Node) {
    @arguments
  }

  def type_arguments -> Array!(TypeNode) {
    @type_arguments
  }

  def instruction? -> Boolean {
    match(let matched = @receiver) {
      as Constant when matched.name == INSTRUCTION_RECEIVER_NAME -> { True }
      else -> { False }
    }
  }

  def block_type -> ?BlockType {
    @block_type
  }

  def block_type=(value: ?BlockType) -> ?BlockType {
    @block_type = value
  }
}

impl Node for Send {
  def location -> SourceLocation {
    @location
  }

  def resolved_type -> ?Type {
    @resolved_type
  }

  def resolved_type=(value: Type) -> Type {
    @resolved_type = value
  }

  def resolved_throw_type -> ?Type {
    @resolved_throw_type
  }

  def resolved_throw_type=(value: Type) -> Type {
    @resolved_throw_type = value
  }
}

impl TryExpressionNode for Send {
  def inside_try {
    @inside_try = True
  }

  def inside_try? -> Boolean {
    @inside_try
  }
}
